import React, { Component } from 'react'
import axios from 'axios'

import Results from './components/results'
import InputQuery from './components/input-query'

class Search extends Component {
  constructor(props) {
    super(props)
    this.state = { data: [] }
    this.update = this.update.bind(this)
  }

  update(value) {
    axios.get(`https://jsonplaceholder.typicode.com/users?username_like=${value}`)
      .then((res) => {
        this.setState({
          data: res.data
        })
      })
  }

  render() {
    const { data } = this.state

    return (
      <div>
        <InputQuery update={this.update} />
        <Results data={data} />
      </div>
    )
  }
}

export default Search
