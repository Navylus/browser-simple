import React, { Component } from 'react'

class Results extends Component {
  render() {
    const { data } = this.props

    return (
      <ul>
        { data.map(person => <li key={person.id}>{person.username}</li>)}
      </ul>
    )
  }
}

export default Results
