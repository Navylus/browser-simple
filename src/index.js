import React from 'react'
import ReactDOM from 'react-dom'
import './index.scss'

import Search from './search'

const App = () => (
  <div>
    <Search />
  </div>
)

ReactDOM.render(<App />, document.getElementById('app'))
